CREATE DATABASE Employee;

USE Employee;

Create Table Employee(
	id INT PRIMARY KEY NOT NULL,
	name VARCHAR(60) NOT NULL,
	lastname VARCHAR(60) NOT NULL,
	salary NUMERIC(10)
);