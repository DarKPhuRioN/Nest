import { Module } from '@nestjs/common';
import { EmployeeController } from './employee.controller';
import { Employee } from './../../providers/employee.provider';
import { EmployeeService } from './employee.service';
import { SharedModule } from './../shared/shared.module';

@Module({
    components: [EmployeeService],
    controllers: [EmployeeController],
    modules: [SharedModule]
})
export class EmployeeModule { 

}