import { Component } from '@nestjs/common'
import { DatabaseService } from './../shared/db.service';

@Component()
export class EmployeeService {

    constructor(
        private db: DatabaseService) {
    }

    public async getAll() {
        return await (this.db.query(`SELECT * FROM Employee`))
    }

    public async getById(id: string){
        return await (this.db.query(`SELECT * FROM Employee WHERE id = ${id}`))
    }

    //no procedure
    public async create(employee) {
        return await (this.db.query(`INSERT INTO Employee VALUES (${employee.id},${employee.name},${employee.lastname},${employee.salary}`))
    }

    //with procedure
    public async createwithProcedure(employee) {
        return await (this.db.query(`CALL insert_Employe (${employee.name},${employee.lastname},${employee.salary})`))
    }

    public async update(id: string, employee) {
        return await (this.db.query(`UPDATE Employee SET name = ${employee.name}, lastname = ${employee.lastname}, salary=${employee.salary} WHERE id = ${id}`))
    }

    public async delete(id: string) {
        return await (this.db.query(`DELETE FROM Employee WHERE id = ${id}`))
    }
}