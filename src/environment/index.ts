import 'dotenv/config'

export const CONFIG = {
    PORT: Number(process.env.PORT) || 3300,
    JWT_SECRET: process.env.JWT_SECRET,
    DB: {
		host: process.env.DBHOST || "localhost",
		port : process.env.DBPORT || 3306/*PORT*/,
		user: process.env.DBUSER || "UserNameDataBase",
		password: process.env.DBPASSWORD || "PasswordDataBase",
		database : process.env.DBNAME || "NameDataBase"
	},
	Key:'YourKey'/*key*/
}
